package com.example.simplemvvm.data;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface PeopleService {

    @GET("people")
    Observable<PeopleResponse> fetchPeople(
            @Query("results") int page,
            @Query("nat") String nat
    );

}
